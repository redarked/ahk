﻿; #NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
; SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
; #InstallKeybdHook

isMage := False

stoCambiandoBersaglio := False
stoCurandomi := False
stoMorendo := False
SetTimer, EliminaLabel, 1000
SetTimer, Login, 600000 ;10 min

#F1::
    starting := True
    while WinActive("CABAL") {
        if (starting) {
            starting := False
            SetTimer, Raccogli, 1000
            SetTimer, Attacca, 1000
        }
        WinGetPos, X, Y, Width, Height, A
        xEnemyHP := Width * 0.43
        yEnemyHP := Height * 0.06
        xCharacterHP := Width * 0.13
        yCharacterHP := Height * 0.055
        xCharacterHPLow := Width * 0.07
        yCharacterHPLow := Height * 0.055
        PixelGetColor, color1, %xEnemyHP%, %yEnemyHP%, RGB
        SplitColor := RGBSplit(color1)
        if (!stoCambiandoBersaglio && (SplitColor.R != 255 || SplitColor.G != 178 || SplitColor.B != 1)) {
            stoCambiandoBersaglio := True
            Random, random, 500, 1500
            SetTimer, CambiaBersaglio, %random%
        }
        PixelGetColor, color2, %xCharacterHP%, %yCharacterHP%, RGB
        SplitColor := RGBSplit(color2)
        if (!stoCurandomi && (SplitColor.R != 255 || SplitColor.G != 28 || SplitColor.B != 19)) {
            stoCurandomi := True
            Gosub, Curami
            SetTimer, Curami, 5500
        }
        if (stoCurandomi && (SplitColor.R == 255 || SplitColor.G == 28 || SplitColor.B == 19)) {
            stoCurandomi := False
            SetTimer, Curami, Off
        }
        PixelGetColor, color3, %xCharacterHPLow%, %yCharacterHPLow%, RGB
        SplitColor := RGBSplit(color3)
        if (!stoMorendo && (SplitColor.R != 255 || SplitColor.G != 28 || SplitColor.B != 19)) {
            stoMorendo := True
            Gosub, Salvami
        }
    }
return
; #z::
;     IfWinActive CABAL
;     {
;         Loop {
;             Send {Space}
;             RandomSleep(20, 200)
;             Send 0
;             RandomSleep(1, 40)
;         }
;     }
; return
; #v::
;     IfWinActive CABAL
;     {
;         Loop {
;             Send {Space}
;             RandomSleep(1, 30)
;         }
;     }
; return
; #a::
;     while WinActive("CABAL") {
;         Send ! 1
;         Sleep 100
;         Send ! 2
;         Sleep 100
;     }
; return
#r::
    ToolTip, riavviato, 100, 150
    Reload
return
^r::
    ToolTip, riavviato, 100, 150
    Reload
return
#c::
    Gosub, Combo
return
#x::
    WinGetPos, X, Y, Width, Height, A
    xPos := Width * 0.55
    yPos := Height * 0.55
    MouseGetPos, mouseX, mouseY
    PixelGetColor, color, %xPos%, %yPos%, RGB
    SplitColor := RGBSplit(color)
    s := "x: " . xPos . ", y: " . yPos . ". mx: " . mouseX . ", my: " . mouseY . " (R: " . SplitColor.R . ", G: " . SplitColor.G . ", B: " . SplitColor.B . ")"
    ToolTip, %s%, Width * 0.5, Height * 0.5
return
#m::
    ToolTip mago, 0, 0
    isMage := True
return

Raccogli:
    if (!WinActive("CABAL")) {
        SetTimer, Raccogli, Off
    } else {
        Send {Space}
    }
return
Attacca:
    if (!WinActive("CABAL")) {
        SetTimer, Attacca, Off
    } else {
        RandomSleep(20, 700)
        RandomSkill(1, 5)
    }
return
AttaccaConBo:
    while WinActive("CABAL") {
        
    }
return
CambiaBersaglio:
    stoCambiandoBersaglio := False
    SetTimer, CambiaBersaglio, Off
    ToolTip CambiaBersaglio, 0, 0
    Send {Tab}
return
Curami:
    Send, 0
    if (isMage) {
        Send ! 7
        Send ! 3
    }
return
Salvami:
    stoMorendo := False
    SetTimer, Salvami, Off
    Send ! 4
    Send ! 8
return
EliminaLabel:
    ToolTip
return
Combo:
    ToolTip combo, 0, 0
    WinGetPos, X, Y, Width, Height, A
    xCombo := Width * 0.54
    yCombo := Height * 0.081
    xEnemyHP := Width * 0.44
    yEnemyHP := Height * 0.06
    combo := 0
    ready := False
    PixelGetColor, color, %xEnemyHP%, %yEnemyHP%, RGB
    SplitColor := RGBSplit(color)
    if (SplitColor.R != 255 || SplitColor.G != 178 || SplitColor.B != 1) {
        Send {Tab}
    }
    Send, '
    while WinActive("CABAL") && combo < 20 {
        PixelGetColor, color, %xCombo%, %yCombo%, RGB
        SplitColor := RGBSplit(color)
        if (ready && SplitColor.R == 255) {
            Send, '
            ready := False
            combo++
            PixelGetColor, color, %xEnemyHP%, %yEnemyHP%, RGB
            SplitColor := RGBSplit(color)
            if (SplitColor.R != 255 || SplitColor.G != 178 || SplitColor.B != 1) {
                Send {Tab}
            }
        } else if (SplitColor.R == 38 && SplitColor.G == 38 && SplitColor.B == 38) {
            ready := True
        }
    }
return
Login:
    WinGetPos, X, Y, Width, Height, A
    x1 := Width * 0.45
    y1 := Height * 0.55
    x2 := Width * 0.55
    y2 := Height * 0.55
    PixelGetColor, color, %x1%, %y1%, RGB
    SplitColor1 := RGBSplit(color)
    PixelGetColor, color, %x2%, %y2%, RGB
    SplitColor2 := RGBSplit(color)
    if ((SplitColor1.R == 20 && SplitColor1.G == 24 && SplitColor1.B == 37 && SplitColor2.R == 22 && SplitColor2.G == 27 && SplitColor2.B == 43)) {
        ToolTip login, 0, 0
        x1 := Width * 0.5
        y1 := Height * 0.56
        x2 := Width * 0.5
        y2 := Height * 0.61
        x3 := Width * 0.9
        y3 := Height * 0.5
        x4 := Width * 0.95
        y4 := Height * 0.95
        MouseClick, left, %x1%, %y1%
        Sleep 1500
        MouseClick, left, %x2%, %y2%
        SendInput, Olmy900{Alt down}{Numpad6}{Numpad4}{Alt up}{Enter}
        Sleep 1500
        MouseClick, left, %x3%, %y3%
        MouseClick, left, %x3%, %y3%
        Sleep 1500
        MouseClick, left, %x4%, %y4%
    }
return

RGBSplit(RGB)
{
    return {"R": (RGB >> 16) & 0xFF, "G": (RGB >> 8) & 0xFF, "B": (RGB) & 0xFF}
}

RandomSleep(min, max)
{
    Random, random, %min%, %max%
    Sleep %random%
}

RandomSkill(min, max)
{
    Random, random, %min%, %max%
    Send %random%
}