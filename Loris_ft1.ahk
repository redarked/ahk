﻿^r::
    Reload
return

RemoveToolTip:
    ToolTip
return

#p::
    paused := !paused
    evaluated := paused ? "ahk in pausa" : "ahk attivo"
    ToolTip, %evaluated%, 0, 0
return

#c::
    WinGetPos, X, Y, Width, Height, A
    ; xPos := 900
    ; yPos := 87
    xPos := Width * 0.54
    yPos := Height * 0.081
    Send, 1
    combo := 0
    ready := False
    counter := 0
    while WinActive("CABAL") &&  !paused {
        PixelGetColor, color, %xPos%, %yPos%, RGB
        SplitColor := RGBSplit(color)
        MouseGetPos, mouseX, mouseY
        if (ready && SplitColor.R > 100) {
            ready := False
            combo++
            Send, 1
            if (combo = 10) {
                xPos := Width * 0.56
            } else if (combo = 20) {
                Break
            }
        } else if (SplitColor.R <= 100) {
            ready := True
        }
    }
return

RGBSplit(RGB)
{
    return {"R": (RGB >> 16) & 0xFF, "G": (RGB >> 8) & 0xFF, "B": (RGB) & 0xFF}
}
